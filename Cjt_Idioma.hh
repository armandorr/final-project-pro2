/** @file Cjt_Idioma.hh
    @brief Especificació de la classe Cjt_Idioma
*/
#ifndef _CJTIDIOMA_HH_
#define _CJTIDIOMA_HH_

#include "Idioma.hh"

/** @class Cjt_Idioma
    @brief Representa un conjunt d'idiomes
    
    Tenim operacions per llegir-lo, per escriure la taula de freqüències, la taula de codis, el bintree, la codificació o decodificació d'un text i per afegir un idioma. 
*/

class Cjt_Idioma{
    
private:
    
    /** @brief Conjunt d'idiomes */
    map<string,Idioma> vidioma;
    
public:
    
    //Constructores
    /** @brief Creadora per defecte.

        S'executa automàticament al declarar un Cjt_Idioma.
        \pre <em>Cert</em>
        \post El resultat es un conjunt d'idiomes buit
    */
    Cjt_Idioma();


    //Modificadores
    /** @brief Afegeix un idioma al paràmetre implícit
        \pre <em>Cert</em>
        \post Si l'idioma ja existeix el modifica sumant o afegint les frequències, en cas contrari l'afegeix al conjunt
    */
    void afegir_Idioma(const string& nom_idioma);
    
    
    //Consultores
    /** @brief Consultora del tamany del paràmetre implícit
        \pre <em>Cert</em>
        \post El resultat indica el tamany del conjunt d'idiomes
    */
    int tamany_Cjt_Idioma() const;


    //Lectura i escriptura
    /** @brief Operació de lectura
        \pre <em>Cert</em>
        \post Hem llegit un conjunt d'idiomes
    */
    void llegir_Cjt_Idioma();
    
    /** @brief Operació d'escriptura de la taula de freqüències d'un idioma
        \pre <em>Cert</em>
        \post Si l'idioma existeix s'ha escrit per pantalla la seva taula de freqüències, en cas contrari se'ns informarà
    */
    void escriure_Taula_freq_Cjt(const string& nom_idioma) const;
    
    /** @brief Operació d'escriptura del bintree d'un idioma
        \pre <em>Cert</em>
        \post Si l'idioma existeix s'ha escrit per pantalla el seu bintree, en cas contrari se'ns informarà
    */
    void escriure_bintree_Cjt(const string& nom_idioma) const;
    
    /** @brief Operació d'escriptura del codi d'un caràcter d'un idioma
        \pre <em>Cert</em>
        \post Si l'idioma existeix i el paràmetre explícit caracter pertany a la taula de codis de l'idioma s'ha escrit per pantalla el seu codi (tots els codis si caracter = "tots"), en cas contrari se'ns informarà
    */
    void escriure_codis_Cjt(const string& nom_idioma, const string& caracter) const;
    
    /** @brief Operació d'escriptura de la codificiació d'un text en un idioma
        \pre <em>Cert</em>
        \post Si l'idioma existeix i el paràmetre explícit text es pot codificar s'ha escrit per pantalla la codificacó, en cas contrari se'ns informarà
    */
    void escriure_codificacio_Cjt(const string& nom_idioma, const string& text) const;
    
    /** @brief Operació d'escriptura de la decodificiació d'un text en un idioma
        \pre El paràmetre explícit text es un string binari (només conté 0's o 1's)
        \post Si l'idioma existeix i el text es pot decodificar s'ha escrit per pantalla la decodificacó, en cas contrari se'ns informarà
    */
    void escriure_decodificacio_Cjt(const string& nom_idioma, const string& text) const;
};

#endif
