﻿#include "Treecode.hh"

//Auxiliar
/** @brief Operador  "<"  sobre BinTree's */
bool operator< (const BinTree< pair<string,int> >& B1, const BinTree< pair<string,int> >& B2)
{ 
    if (B1.value().second != B2.value().second) return B1.value().second < B2.value().second;
    return B1.value().first < B2.value().first;
}

//Mètodes privats
void Treecode::fer_bintree_codis(set< BinTree< pair<string,int> > >& S, BinTree< pair<string,int> >& arbre, map <string,string>& codis)
{
    int n = S.size() - 1;
    while (n > 0){
        set< BinTree< pair<string,int> > >::const_iterator it1 = S.begin(), it2 = S.begin();
        ++it2;
        
        string s1 = (*it1).value().first;
        string s2 = (*it2).value().first;
        if (s1 > s2) swap(s1, s2);
        s1 += s2;
        
        pair<string,int> P (s1, (*it1).value().second + (*it2).value().second);
        BinTree< pair<string,int> > B(P, *it1, *it2);

        S.erase(it1);
        S.erase(it2);
        S.insert(B);
        --n;
    }
    arbre = *(S.begin());
    
    string codi_caracter = "";
    fer_codis(arbre, codis, codi_caracter);
}

void Treecode::fer_codis(const BinTree< pair<string,int> >& T, map <string,string>& codis_original, string codi)
{
    if (not T.empty()){
        if (T.left().empty()) codis_original[T.value().first] = codi;
        else{
            fer_codis(T.left(), codis_original, codi + "0");
            fer_codis(T.right(), codis_original, codi + "1");
        }
    }
}

void Treecode::escriure_preorder(const BinTree< pair<string,int> >& a)
{
    if (not a.empty()){
        cout << a.value().first << " " << a.value().second << endl;
        escriure_preorder (a.left());
        escriure_preorder (a.right());
    }
}

void Treecode::escriure_inorder(const BinTree< pair<string,int> >& a)
{
    if (not a.empty()){
        escriure_inorder(a.left());
        cout << a.value().first << " " << a.value().second << endl;
        escriure_inorder(a.right());
    }
}

bool Treecode::decodificable(const BinTree< pair<string,int> >& arbre, const string& text, string& decodificat, int& ultima_pos, int& pos) const
{
    if (arbre.left().empty()){
        decodificat += arbre.value().first;
        ultima_pos = pos;
        return pos == text.size() or decodificable(bintree, text, decodificat, ultima_pos, pos); 
    }
    if (pos == text.size()) return false;
    if (text[pos] == '0'){
        return decodificable(arbre.left(), text, decodificat, ultima_pos, ++pos);
    }
    return decodificable(arbre.right(), text, decodificat, ultima_pos, ++pos);
}

//Constructores
Treecode::Treecode(){}

//Modificadores
void Treecode::fer_Treecode(Taula_freq& T)
{
    set< BinTree< pair<string,int> > > S;
    pair<string,int> P;

    T.inicialitzar();
    for (int i = 0; i < T.tamany_Taula_freq(); ++i)
    {
        P.first = T.consultar_caracter();
        P.second = T.consultar_freq();
        S.insert(BinTree< pair<string,int> > (P));
    }
    
    fer_bintree_codis(S, bintree, taula_codis);
}

//Consultores
bool Treecode::codificable(const string& text, string& caracter_falla, string& codificat) const
{
    string caracter;
    string codi;
    int i = 0;
    while(i < text.size()){
        caracter = text[i];
        if (text[i] < 0){
            ++i;
            caracter += text[i];
        }
        map<string,string>::const_iterator it = taula_codis.find(caracter);
        if (it == taula_codis.end()){
            caracter_falla = caracter;
            return false;
        }
        codificat += (*it).second;
        ++i;
    }
    return true;
}

//Lectura i escriptura
void Treecode::escriure_bintree_Treecode() const
{
    cout << "recorrido en preorden:" << endl;
    escriure_preorder(bintree);
    cout << "recorrido en inorden:" << endl;
    escriure_inorder(bintree);
    cout << endl;
}

void Treecode::escriure_codis_Treecode(const string& caracter) const
{
    if (caracter == "todos"){
        for (map<string,string>::const_iterator it = taula_codis.begin(); it != taula_codis.end(); ++it){
            cout << (*it).first << " " << (*it).second << endl;
        }
    }
    else{
        map<string,string>::const_iterator it = taula_codis.find(caracter);
        if (it != taula_codis.end()) cout << caracter << " " << (*it).second << endl;
        else cout << "El idioma no existe o el caracter no esta en el idioma" << endl;
    }
    cout << endl;
}

void Treecode::escriure_codificacio_Treecode(const string& text) const
{
    string falla;
    string codificacio = "";
    
    if (codificable(text, falla, codificacio)) cout << codificacio << endl << endl;
    else cout << "El texto no pertenece al idioma; primer caracter que falla: " << falla << endl << endl;
}

void Treecode::escriure_decodificacio_Treecode(const string& text) const
{
    string decodificat = "";
    int posicio = 0, ultima_pos_decodificable = 0;

    if (decodificable(bintree, text, decodificat, ultima_pos_decodificable, posicio)) cout << decodificat << endl << endl;
    else cout << "El texto no procede de una codificacion del idioma; ultima posicion del codigo correspondiente al ultimo caracter que se podria decodificar: " << ultima_pos_decodificable << endl << endl;
}
