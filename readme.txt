To compile:
	# make
	(The file Makefile will automathically do all the work)

To execute:
	# ./program.exe <sample.inp >sample.out
	(You can use the file sample.inp as an input file)
	(That code will generate a sample.out file with the result)
	
	
To generate a doxygen documentation:
	# doxygen Doxyfile
	(You will get a DOC folder with the html and a possible compilable latex pdf)
	

	