/** @file Treecode.hh
    @brief Especificació de la classe Treecode
*/
#ifndef _TREECODE_HH_
#define _TREECODE_HH_

#ifndef NO_DIAGRAM
#include <set>
#include "BinTree.hh"
#endif

#include "Taula_freq.hh"

/** @class Treecode
    @brief Representa un arbre treecode i una taula de codis
    
    Tenim operacions per construir la taula de codis i el arbre treecode, per consultar un codi, per saber la codificabilitat o decodificabilitat d'un text i diverses operacions d'escriptura.
*/

class Treecode{

private:
    
    /** @brief Arbre treecode */
    BinTree < pair<string,int> > bintree;

    /** @brief Taula de codis */
    map < string,string > taula_codis;

    /** @brief Operació que fa el arbre treecode i la taula de codis
        \pre S no és buit
        \post S'ha fet el arbre treecode i tots els codis
    */
    static void fer_bintree_codis(set< BinTree< pair<string,int> > >& S, BinTree< pair<string,int> >& arbre, map <string,string>& codis);
    
    /** @brief Operació recursiva que fa tots els codis 
        \pre codi = ""
        \post S'ha omplert el paràmetre explícit codis amb tots els codis
    */
    static void fer_codis(const BinTree< pair<string,int> >& T, map <string,string>& codis, string codi);
    
    /** @brief Operació recursiva que escriu el arbre treecode en preordre
        \pre <em>Cert</em>
        \post S'ha escrit per pantalla el recorregut en preordre del arbre treecode
    */
    static void escriure_preorder(const BinTree < pair<string,int> >& a);
    
    /** @brief Operació recursiva que escriu el arbre treecode en inordre
        \pre <em>Cert</em>
        \post S'ha escrit per pantalla el recorregut en inordre del arbre treecode
    */
    static void escriure_inorder(const BinTree < pair<string,int> >& a);
    
    /** @brief Operació consultora de la decodificabilitat d'un text
        \pre decodificat = "" i 0 <= pos <= text.size()
        \post El resultat indica si el text es decodificable. Si ho és tenim guardat a decodificat la decodificació del text, en cas contrari, tenim a ultima_pos la posició de l'últim tros de text decodificable
    */
    bool decodificable(const BinTree< pair<string,int> >& arbre, const string& text, string& decodificat, int& ultima_pos ,int& pos) const;

public:
    
    //Constructores
    /** @brief Creadora per defecte

        S'executa automàticament al declarar un arbre treecode.
        \pre <em>Cert</em>
        \post El resultat es un arbre treecode que conté un arbre treecode i una taula de codis
    */
    Treecode();
    
    
    //Modificadores
    /** @brief Operació que fa el arbre treecode i tots els codis a partir d'una taula de freqüències
        \pre T no buida
        \post S'ha fet el arbre treecode i tots els codis
    */
    void fer_Treecode(Taula_freq& T);
    
    
    //Consultores
    /** @brief Operació consultora de la codificabilitat del text
        \pre <em>Cert</em>
        \post El resultat indica si el text es codificable. Si ho és tenim guardat a codificat la codificació del text, en cas contrari, tenim a caracter_falla el primer caràcter on la codificació falla
    */
    bool codificable(const string& text, string& caracter_falla, string& codificat) const;
    
    
    //Lectura i escriptura
    /** @brief Operació d'escriptura del arbre treecode
        \pre <em>Cert</em>
        \post S'ha escrit per pantalla el arbre treecode amb recorregut en preordre i en inordre
    */
    void escriure_bintree_Treecode() const;
    
    /** @brief Operació d'escriptura del codi d'un caracter
        \pre <em>Cert</em>
        \post Si el paràmetre explícit caracter pertany a la taula de codis s'ha escrit per pantalla el codi de caracter (tots els codis si caracter = "tots"), en cas contrari se'ns informarà
    */
    void escriure_codis_Treecode(const string& caracter) const;
    
    /** @brief Operació d'escriptura de la codificiació d'un text
        \pre <em>Cert</em>
        \post Si el text es pot codificar s'ha escrit per pantalla la codificació, en cas contrari se'ns informarà
    */
    void escriure_codificacio_Treecode(const string& text) const;
    
    /** @brief Operació d'escriptura de la decodificiació d'un text
        \pre El paràmetre explícit text es un string binari (només conté 0's o 1's)
        \post Si el text binari es pot decodificar s'ha escrit per pantalla la decodificació, en cas contrari se'ns informarà
    */
    void escriure_decodificacio_Treecode(const string& text) const;
};

#endif
