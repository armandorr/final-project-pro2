﻿/** @mainpage Pràctica PRO2: Gestió d'un conjunt d'idiomes.

 Es construeix un programa modular que ofereix un menu d'opcions per gestionar un conjunt d'idiomes. S'introdueixen les classes <em>Cjt_Idioma</em>,  <em>Idioma</em>,  <em>Taula_freq</em>  i  <em>Treecode</em>
*/

/** @file program.cc
    @brief Programa principal de la pràctica de PRO2, Primavera 2019. Entrega definitiva.
*/

#include "Cjt_Idioma.hh"

/** @brief Programa principal de la pràctica de PRO2.
*/

int main()
{
    Cjt_Idioma C;
    C.llegir_Cjt_Idioma();
    string key, nom_idioma, text;
    
    cin >> key;
    while (key != "fin"){

        if (key == "anadir/modificar"){
            cin >> nom_idioma;
            C.afegir_Idioma(nom_idioma);
        }
        
        else if (key == "tabla_frec"){
            cin >> nom_idioma;
            C.escriure_Taula_freq_Cjt(nom_idioma);
        }
        
        else if (key == "treecode"){
            cin >> nom_idioma;
            C.escriure_bintree_Cjt(nom_idioma);
        }
        
        else if (key == "codigos"){
            cin >> nom_idioma >> text;
            C.escriure_codis_Cjt(nom_idioma,text);
        }
        
        else if (key == "codifica"){
            cin >> nom_idioma >> text;
            C.escriure_codificacio_Cjt(nom_idioma, text);
        }
        
        else if (key == "decodifica"){
            cin >> nom_idioma >> text;
            C.escriure_decodificacio_Cjt(nom_idioma, text);
        }
        
        cin >> key;
    }
}
