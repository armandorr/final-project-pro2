#include "Taula_freq.hh"

//Constructores
Taula_freq::Taula_freq()
{
    iterador = Taula_F.begin();
}

//Modificadores
void Taula_freq::fer_modificar_Taula_freq()
{
    string caracter;
    int numero_freq, freq;
    cin >> numero_freq;
    for (int i = 0; i < numero_freq; ++i){
        cin >> caracter >> freq;
        Taula_F[caracter] += freq;
    }
}
  
void Taula_freq::inicialitzar()
{
    iterador = Taula_F.begin();
}

//Consultores
int Taula_freq::tamany_Taula_freq() const
{
    return Taula_F.size();
}

string Taula_freq::consultar_caracter() const
{
    return (*iterador).first;
}

int Taula_freq::consultar_freq()
{
    int n = (*iterador).second;
    ++iterador;
    return n;
}
    
//Lectura i escriptura    
void Taula_freq::escriure_Taula_freq() const
{
    for (map< string,int >::const_iterator it = Taula_F.begin(); it != Taula_F.end(); ++it)
    {
        cout << (*it).first << " " << (*it).second << endl;
    }
    cout << endl;
}

