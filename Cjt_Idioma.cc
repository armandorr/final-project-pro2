#include "Cjt_Idioma.hh"

//Constructores
Cjt_Idioma::Cjt_Idioma(){}

//Modificadores
void Cjt_Idioma::afegir_Idioma(const string& nom_idioma)
{
    map<string,Idioma>::iterator it = vidioma.find(nom_idioma);
    if (it == vidioma.end()){
        cout << "Anadido " << nom_idioma << endl << endl;
        vidioma[nom_idioma].llegir_Idioma();
    }
    else{
        cout << "Modificado " << nom_idioma << endl << endl;
        ((*it).second).llegir_Idioma();
    }
}

//Consultores
int Cjt_Idioma::tamany_Cjt_Idioma() const
{
    return vidioma.size();
}

//Lectura i escriptura
void Cjt_Idioma::llegir_Cjt_Idioma()
{
    string nom_idioma;
    int nidioma;
    cin >> nidioma;
    
    for (int i = 0; i < nidioma; ++i)
    {
        cin >> nom_idioma;
        vidioma[nom_idioma].llegir_Idioma();
    }
}

void Cjt_Idioma::escriure_Taula_freq_Cjt(const string& nom_idioma) const
{
    cout << "Tabla de frecuencias de " << nom_idioma << ":" << endl;
    map<string,Idioma>::const_iterator it = vidioma.find(nom_idioma);
    if (it != vidioma.end()) ((*it).second).escriure_Taula_freq_Idioma();
    else cout << "El idioma no existe" << endl << endl;
}

void Cjt_Idioma::escriure_bintree_Cjt(const string& nom_idioma) const
{
    cout << "Treecode de " << nom_idioma << ":" << endl;
    map<string,Idioma>::const_iterator it = vidioma.find(nom_idioma);
    if (it != vidioma.end()) ((*it).second).escriure_bintree_Idioma();
    else cout << "El idioma no existe" << endl << endl;
}

void Cjt_Idioma::escriure_codis_Cjt(const string& nom_idioma, const string& caracter) const
{
    if (caracter == "todos") cout << "Codigos de " << nom_idioma << ":" << endl;
    else cout << "Codigo de " << caracter << " en " << nom_idioma << ":" << endl;
    map<string,Idioma>::const_iterator it = vidioma.find(nom_idioma);
    if (it == vidioma.end()){
        cout << "El idioma no existe";
        if (caracter != "todos") cout << " o el caracter no esta en el idioma";
        cout << endl << endl;
    }
    else ((*it).second).escriure_codis_Idioma(caracter);
}

void Cjt_Idioma::escriure_codificacio_Cjt(const string& nom_idioma, const string& text) const
{
    cout << "Codifica en " << nom_idioma << " el texto:" << endl << text << endl;
    map<string,Idioma>::const_iterator it = vidioma.find(nom_idioma);
    if (it != vidioma.end()) ((*it).second).escriure_codificacio_Idioma(text);
    else cout << "El idioma no existe" << endl << endl;
}
    
void Cjt_Idioma::escriure_decodificacio_Cjt(const string& nom_idioma, const string& text) const
{
    cout << "Decodifica en " << nom_idioma << " el texto:" << endl << text << endl;
    map<string,Idioma>::const_iterator it = vidioma.find(nom_idioma);
    if (it != vidioma.end()) ((*it).second).escriure_decodificacio_Idioma(text);
    else cout << "El idioma no existe" << endl << endl;
}
