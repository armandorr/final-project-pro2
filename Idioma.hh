/** @file Idioma.hh
    @brief Especificació de la classe Idioma 
*/
#ifndef _IDIOMA_HH_
#define _IDIOMA_HH_

#include "Taula_freq.hh"
#include "Treecode.hh"

/** @class Idioma
    @brief Representa un idioma amb una taula de freqüències, un bintree i una taula de codis
    
    Tenim una operació de lectura i diverses operacions d'escriptura, com per exemple, l'escriptura de la taula de freqüències o de la decodificació d'un text.
*/

class Idioma{

private:
    
    /** @brief Taula de freqüències */
    Taula_freq Taula;
    
    /** @brief Bintree i taula de codis */
    Treecode Tree;
    
public:
    
    //Constructores
    /** @brief Creadora per defecte

        S'executa automàticament al declarar un Idioma.
        \pre <em>Cert</em>
        \post El resultat es un idioma amb una taula de freqüències, un bintree i una taula de codis buits
    */
    Idioma();


    //Modificadores


    //Consultores


    //Lectura i escriptura
    /** @brief Operació de lectura
        \pre <em>Cert</em>
        \post Hem llegit un idioma (la seva taula de freqüències), ja existent o no, i em creat el bintree i la taula de codis respectius
    */
    void llegir_Idioma();
    
    /** @brief Operació d'escriptura de la taula de freqüències del paràmetre implícit
        \pre <em>Cert</em>
        \post S'ha escrit per pantalla la taula de freqüències
    */
    void escriure_Taula_freq_Idioma() const;
    
    /** @brief Operació d'escriptura del bintree del paràmetre implícit
        \pre <em>Cert</em>
        \post S'ha escrit per pantalla el bintree
    */
    void escriure_bintree_Idioma() const;
    
    /** @brief Operació d'escriptura del codi d'un caràcter del paràmetre implícit
        \pre <em>Cert</em>
        \post Si el paràmetre explícit caracter pertany a la taula de codis del paràmetre implícit s'ha escrit per pantalla el seu codi (tots els codis si caracter = "tots"), en cas contrari se'ns informarà
    */
    void escriure_codis_Idioma(const string& caracter) const;
    
    /** @brief Operació d'escriptura de la codificiació d'un text
        \pre <em>Cert</em>
        \post Si el text es pot codificar s'ha escrit per pantalla la seva codificació, en cas contrari se'ns informarà
    */
    void escriure_codificacio_Idioma(const string& text) const;
    
    /** @brief Operació d'escriptura de la decodificiació d'un text
        \pre El paràmetre explícit text es un string binari (només conté 0's o 1's)
        \post Si el text es pot decodificar s'ha escrit per pantalla la seva decodificació, en cas contrari se'ns informarà
    */
    void escriure_decodificacio_Idioma(const string& text) const;
};

#endif
