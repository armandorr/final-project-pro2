OPCIONS = -D_JUDGE_ -D_GLIBCXX_DEBUG -O2 -Wall -Wextra -Werror -Wno-sign-compare -std=c++11

program.exe: program.o Cjt_Idioma.o Idioma.o Taula_freq.o Treecode.o
	g++ -o program.exe *.o $(OPCIONS)

program.o: program.cc Cjt_Idioma.hh Idioma.hh Taula_freq.hh Treecode.hh
	g++ -c program.cc $(OPCIONS)

Cjt_Idioma.o: Cjt_Idioma.cc Cjt_Idioma.hh
	g++ -c Cjt_Idioma.cc $(OPCIONS)

Idioma.o: Idioma.cc Idioma.hh
	g++ -c Idioma.cc $(OPCIONS)

Taula_freq.o: Taula_freq.cc Taula_freq.hh
	g++ -c Taula_freq.cc $(OPCIONS)

Treecode.o: Treecode.cc Treecode.hh
	g++ -c Treecode.cc $(OPCIONS)

practica.tar: program.cc Cjt_Idioma.cc Idioma.cc Taula_freq.cc Treecode.cc Cjt_Idioma.hh Idioma.hh Taula_freq.hh Treecode.hh Makefile html.zip
	tar -cvf practica.tar program.cc Cjt_Idioma.cc Idioma.cc Taula_freq.cc Treecode.cc Cjt_Idioma.hh Idioma.hh Taula_freq.hh Treecode.hh Makefile html.zip

clean:
	rm -f *.o
	rm -f *.exe
	rm -f *.out

