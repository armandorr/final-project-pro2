/** @file Taula_freq.hh
    @brief Especificació de la classe Taula_freq
*/
#ifndef _TAULAFREQ_HH_
#define _TAULAFREQ_HH_

#ifndef NO_DIAGRAM
#include <iostream>
#include <map>
#endif

using namespace std;

/** @class Taula_freq
    @brief Representa una taula de dades amb les seves corresponents freqüències
    
    Tenim operacions per fer i modificar la taula, per consultar-ne el tamany, per consultar-la progressivament i una operació d'escriptura.
*/

class Taula_freq{
    
private:
    
    /** @brief Taula de freqüències */
    map <string,int> Taula_F;
    
    /** @brief Punt d'interés de la taula de freqüències */
    map <string,int>::const_iterator iterador;
    
public:
    
    //Constructores
    /** @brief Creadora per defecte

        S'executa automàticament al declarar una Taula_freq.
        \pre <em>Cert</em>
        \post El resultat es una taula de freqüències buida on el punt d'interés és la primera posició de la taula
    */
    Taula_freq();
    
    
    //Modificadores
    /** @brief Operació que modifica el paràmetre implícit
        \pre <em>Cert</em>
        \post S'ha modificat la taula de freqüències, afegint les dades no existents i sumant les noves freqüències a les ja existents
    */
    void fer_modificar_Taula_freq();
    
    /** @brief Operació que inicialitza el punt d'interés del paràmetre implícit
        \pre <em>Cert</em>
        \post El punt d'interés és el principi de la taula
    */
    void inicialitzar();
    
    
    //Consultores
    /** @brief Operació consultora del tamany del paràmetre implícit
        \pre <em>Cert</em>
        \post El resultat es el tamany de la taula de freqüències
    */
    int tamany_Taula_freq() const;
    
    /** @brief Operació consultora d'un caracter
        \pre El punt d'interés pertany a una posició accesible de la taula
        \post El resultat es el caracter del punt d'interés de la taula
    */
    string consultar_caracter() const;
    
    /** @brief Operació consultora d'una freqüència
        \pre El punt d'interés pertany a una posició accesible de la taula
        \post El resultat es la freqüència del punt d'interés de la taula, el punt d'interés augmentarà una posició
    */
    int consultar_freq();
    
    
    //Lectura i escriptura
    /** @brief Operació d'escriptura
        \pre <em>Cert</em>
        \post S'ha escrit per pantalla la taula de freqüències
    */
    void escriure_Taula_freq() const;
};

#endif
