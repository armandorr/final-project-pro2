#include "Idioma.hh"

//Constructores
Idioma::Idioma(){}

//Lectura i escriptura
void Idioma::llegir_Idioma()
{
    Taula.fer_modificar_Taula_freq();
    Tree.fer_Treecode(Taula);
}

void Idioma::escriure_Taula_freq_Idioma() const
{
    Taula.escriure_Taula_freq();
}

void Idioma::escriure_bintree_Idioma() const
{
    Tree.escriure_bintree_Treecode();
}

void Idioma::escriure_codis_Idioma(const string& text) const
{
    Tree.escriure_codis_Treecode(text);
}

void Idioma::escriure_codificacio_Idioma(const string& text) const
{
    Tree.escriure_codificacio_Treecode(text);
}
    
void Idioma::escriure_decodificacio_Idioma(const string& text) const
{
    Tree.escriure_decodificacio_Treecode(text);
}
